<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikedislikepertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likedislikepertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('poin');

            $table->unsignedBigInteger('pertanyaan1_id');
            $table->foreign('pertanyaan1_id')->references('id')->on('pertanyaan');

            $table->unsignedBigInteger('user1_id');
            $table->foreign('user1_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likedislikepertanyaan');
    }
}
