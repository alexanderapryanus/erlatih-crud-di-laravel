<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikedislikejawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likedislikejawaban', function (Blueprint $table) {
            //$table->bigIncrements('jawaban1_id');
            //$table->unsignedBigInteger('user1_id');
            $table->integer('poin');

            $table->unsignedBigInteger('jawaban1_id');
            $table->foreign('jawaban1_id')->references('id')->on('jawaban');

            $table->unsignedBigInteger('user1_id');
            $table->foreign('user1_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likedislikejawaban');
    }
}
