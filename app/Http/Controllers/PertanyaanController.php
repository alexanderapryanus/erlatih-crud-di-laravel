<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        $query = DB:: table('pertanyaan')->insert([
            "judul"=> $request["judul"],
            "isi"=> $request["isi"]
        ]);

        return redirect('/pertanyaan/create');

    }
}
